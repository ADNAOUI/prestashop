<?php

class PasswordController extends PasswordControllerCore
{

    public function postProcess()
    {
        if (Tools::isSubmit('email')) {
            if (!($email = trim(Tools::getValue('email'))) || !Validate::isEmail($email))
                return $this->errors[] = Tools::displayError('Invalid email address.');

            $customer = new Customer();
            $customer->getByEmail($email);
            $today = strtotime(date('Y-m-d'));

            if ((int)$customer->password_reset_count >= 2 && strtotime($customer->last_passwd_gen) == $today)
                return $this->errors[] = Tools::displayError('Vous avez atteint le nombre maximum de demande de réinitialisation de mot de passe pour aujourd\'hui.');

            if (!Validate::isLoadedObject($customer) || !$customer->active)
                return $this->context->smarty->assign(['confirmation' => 3]);

            $token                          = md5(uniqid(rand(), true));
            $customer->password_reset_token = $token;
            $customer->password_reset_count++;
            $customer->last_passwd_gen = date('Y-m-d');
            $customer->update();

            $mail_params = [
                '{email}'     => $customer->email,
                '{lastname}'  => $customer->lastname,
                '{firstname}' => $customer->firstname,
                '{url}'       => $this->context->link->getPageLink('reset-password', true, null, 'token=' . $customer->password_reset_token)
            ];

            if (Mail::Send(
                $this->context->language->id,
                'password_query',
                Mail::l('Password query confirmation'),
                $mail_params,
                $customer->email,
                $customer->firstname . ' ' . $customer->lastname
            )) {
                return $this->context->smarty->assign(['confirmation' => 2]);
            }

            return $this->errors[] = Tools::displayError('An error occurred while sending the email.');
        }
    }

    public function initContent()
    {
        parent::initContent();

          // if ($this->ajax)
          //     $this->display();
    }
}
