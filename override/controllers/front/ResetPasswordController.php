<?php

class ResetPasswordController extends FrontController
{
    public function initContent()
    {
        parent::initContent();

        $this->setTemplate(_PS_THEME_DIR_ . 'reset-password.tpl');
    }

    public function postProcess()
    {
        $token = Tools::getValue('token');
        $email = Customer::getEmailByTokenAndId($token);

        if (!$email)
            return $this->errors[] = Tools::displayError('We cannot regenerate your password with the data you\'ve submitted.');

        if ($email && Tools::isSubmit("newCredentials")) {
            $new_password     = Tools::getValue('new_password');
            $confirm_password = Tools::getValue('confirm_password');

            $customer = new Customer();
            $customer->getByemail($email);

            if (!Validate::isLoadedObject($customer) || !$customer->active)
                return $this->errors[] = Tools::displayError('You cannot regenerate the password for this account.');


            if (!Validate::isPasswd($new_password))
                return $this->errors[] = Tools::displayError('cas d\'erreur 1');

            if ($new_password != $confirm_password)
                return $this->errors[] = Tools::displayError('cas d\'erreur 2');

            $customer->passwd               = Tools::encrypt($new_password);
            $customer->password_reset_token = '';
            $customer->password_reset_count = 0;
            $customer->last_passwd_gen      = date('Y-m-d');
            $customer->update();

            $this->context->smarty->assign([
                'passwordIsReset' => 1,
                'link_acount'  => Context::getContext()->link->getPageLink('my-account', true)
            ]);

            //envoie d'un mail pour confirmation du changement de mot de passe

            // $mail_params = array(
            //     '{email}' => $customer->email,
            //     '{lastname}' => $customer->lastname,
            //     '{firstname}' => $customer->firstname,
            // );
            // Mail::Send(
            //     $this->context->language->id,
            //     'password_query',
            //     Mail::l('Password query confirmation'),
            //     $mail_params,
            //     $customer->email,
            //     $customer->firstname . ' ' . $customer->lastname
            // );
        }
    }


    public function setMedia()
    {
        parent::setMedia();

        $this->addJS([
            _THEME_JS_DIR_ . 'authentication.js',
        ]);
    }
}
