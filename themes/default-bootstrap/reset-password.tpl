{include file="$tpl_dir./errors.tpl"}

{if isset($passwordIsReset) && $passwordIsReset}
    <div class="alert alert-success" role="alert">
        <p>
            <span>Mot de passé ok !!</span>
            <a href="{$link_acount}">me connecter à mon espace</a>
        </p>
    </div>
{else }
    <form method="post" id="generateNewPassword" onsubmit='password.submitFormReset(event, this)'>

        <div class='form-group '>
            <label for="new_password">Nouveau mot de passe :</label>
            <input type="password" id="new_password" name="new_password" required>
        </div>

        <div class='form-group'>
            <label for="confirm_password">Confirmer le mot de passe :</label>
            <input type="password" id="confirm_password" name="confirm_password" required>
        </div>

        <input type="submit" name='newCredentials' value="Enregistrer">
    </form>
{/if}