$(document).ready(function () {
	$(document).on('submit', '#create-account_form', function (e) {
		e.preventDefault();
		submitFunction();
	});
	$('.is_customer_param').hide();
});

function submitFunction() {
	$('#create_account_error').html('').hide();
	$.ajax({
		type: 'POST',
		url: baseUri + '?rand=' + new Date().getTime(),
		async: true,
		cache: false,
		dataType: "json",
		headers: {
			"cache-control": "no-cache"
		},
		data: {
			controller: 'authentication',
			SubmitCreate: 1,
			ajax: true,
			email_create: $('#email_create').val(),
			back: $('input[name=back]').val(),
			token: token
		},
		success: function (jsonData) {
			if (jsonData.hasError) {
				var errors = '';
				for (error in jsonData.errors)
					//IE6 bug fix
					if (error != 'indexOf')
						errors += '<li>' + jsonData.errors[error] + '</li>';
				$('#create_account_error').html('<ol>' + errors + '</ol>').show();
			} else {
				// adding a div to display a transition
				$('#center_column').html('<div id="noSlide">' + $('#center_column').html() + '</div>');
				$('#noSlide').fadeOut('slow', function () {
					$('#noSlide').html(jsonData.page);
					$(this).fadeIn('slow', function () {
						if (typeof bindUniform !== 'undefined')
							bindUniform();
						if (typeof bindStateInputAndUpdate !== 'undefined')
							bindStateInputAndUpdate();
						document.location = '#account-creation';
					});
				});
			}
		},
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			error = "TECHNICAL ERROR: unable to load form.\n\nDetails:\nError thrown: " + XMLHttpRequest + "\n" + 'Text status: ' + textStatus;
			if (!!$.prototype.fancybox) {
				$.fancybox.open([{
					type: 'inline',
					autoScale: true,
					minHeight: 30,
					content: "<p class='fancybox-error'>" + error + '</p>'
				}], {
					padding: 0
				});
			} else
				alert(error);
		}
	});
}

var password = {
	submitFormReset: function (event, target) {

		const form = document.forms[target.id]
		const new_password = form.elements["new_password"]
		const confirm_password = form.elements["confirm_password"]

		const patternPassword = /^(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/;

		//check validity values
		for (var i = 0; i < form.elements.length; i++) {
			var input = form.elements[i];

			if (input.type === "password") {

				input.parentElement.classList.remove('form-error');

				if (input.value === '')
					input.parentElement.classList.add('form-error');

				//test regex validation mail js
				if (!patternPassword.test(input.value))
					input.parentElement.classList.add('form-error');

				if (new_password.value != confirm_password.value) {
					new_password.parentElement.classList.add("form-error")
					confirm_password.parentElement.classList.add("form-error")
				}

				if (input.parentElement.classList.contains("form-error"))
					return event.preventDefault();
			}
		}
	}
}